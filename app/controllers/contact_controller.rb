class ContactController < ApplicationController
  def index 
  end

  def new
   
  end

  def edit
  end
  
  def create     
    email = params[:contact][:email]
    message = params[:contact][:message]
    @debug = email 
    UserMailer.send_mail(email, message).deliver_now
    
    flash[:info] = "Thanks for your feedback!!"
    redirect_to root_url
    
  end

end
